import React, { Component } from 'react';
import { connect } from 'react-redux'
import logo from './logo2x.png';
import './App.css';
import Button from 'react-toolbox/lib/button/Button';
import ConnectedSortableTaskList from "./components/SortableTaskList";
import {addCard, getCards, postCards, saveCards} from "./actions/cardActions";



class App extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getCards()
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                </div>
                <div className="Task-container">
                    <div className="Task-container-header">
                        <span className="title">
                            Tasks
                        </span>
                        <span className="buttons">
                            <Button
                                className="add-button"
                                label='Add Task'
                                flat primary
                                onClick={()=> this.props.addCard()}
                            />
                            <Button
                                className="save-button"
                                label='Save'
                                flat primary
                                onClick={()=> this.props.postCards()}
                            />
                        </span>
                    </div>
                    <ConnectedSortableTaskList/>
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addCard: () => dispatch(addCard()),
        postCards: (tasks) => dispatch(postCards(tasks)),
        getCards: () => dispatch(getCards())
    }
}

const ConnectedApp = connect(null, mapDispatchToProps)(App);

export default ConnectedApp;
