import React from 'react';
import ConnectedApp from '../App';
import { expect } from 'chai';
import {mount, render, shallow} from 'enzyme';

import configureStore from 'redux-mock-store'
import { connect } from 'react-redux';
import { shallowWithStore } from 'enzyme-redux';
import { createMockStore } from 'redux-test-utils';

import Card from "react-toolbox/lib/card/Card";
import CardTitle from "react-toolbox/lib/card/CardTitle";
// import EditableTitle from "../components/EditableTitle.js";


describe('<App>', () => {

    const initialState = {cards: [{task: "task1"}, {task: "task2"}]};
    const mockStore = configureStore();
    let store,wrapper;

    beforeEach(()=>{
        store = mockStore(initialState);
        wrapper = shallow(<ConnectedApp store={store} />).dive()
    });

    it('renders App without crashing', () => {
        expect(wrapper).to.have.length(1)
    });

    describe('task cards', () => {
        describe('on load', () => {
            it('displays two task cards', () => {
                expect(wrapper.find(Card)).to.have.length(2);
            });

            it('the task label is not editable on load', () => {
                expect(wrapper.find(CardTitle)).to.have.length(2);
            });

            it('displays the correct text per the store', () => {
                expect(wrapper.find(CardTitle).at(0).props()).to.have.property('title', initialState.cards[0].task);
                expect(wrapper.find(CardTitle).at(1).props()).to.have.property('title', initialState.cards[1].task);
            });

        });
        describe('on click title', () => {
            it('it becomes editable', () => {
                expect(wrapper.find(CardTitle)).to.have.length(2);
                wrapper.find(CardTitle).at(1).simulate('click');

                expect(wrapper.find(EditableTitle)).to.have.length(1);
                expect(wrapper.find(CardTitle)).to.have.length(1);
            });
            it('it displays the correct text per the store', () => {
                // expect(wrapper.find(CardTitle).at(0).props()).to.have.property('title', initialState.cards[0].task);
                // expect(wrapper.find(CardTitle).at(1).props()).to.have.property('title', initialState.cards[1].task);
            });
        });
    });

    // describe(('add new task button'), () => {
    //     it('set edtiable label in focus when adding', () => {
    //         //TODO
    //     });
    // });
});