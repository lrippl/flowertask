import {
    ADD_CARD, CARD_ARRAY_MOVE, EDIT_CARD_TITLE, FETCHING_CARDS, FETCHING_CARDS_FAILURE, FETCHING_CARDS_SUCCESS,
    REMOVE_CARD, SAVE_CARDS, SAVE_CARDS_FAILURE, SAVE_CARDS_SUCCESS,
    SET_CARD_TITLE
} from '../constants';

export function editCardTitle(index){
    return {
        type: EDIT_CARD_TITLE,
        index: index
    }
}

export function addCard(){
    return {
        type: ADD_CARD
    }
}
export function removeCard(index){
    return {
        type: REMOVE_CARD,
        index: index
    }
}

export function setCardTitle(text, index){
    return {
        type: SET_CARD_TITLE,
        text: text,
        index: index
    }
}

export function cardArrayMove(oldIndex, newIndex){
    return {
        type: CARD_ARRAY_MOVE,
        oldIndex: oldIndex,
        newIndex: newIndex
    }
}







export function fetchCards(){
    return ({
        type: FETCHING_CARDS
    })
}

export function fetchCardsSuccess(data){
    return {
        type: FETCHING_CARDS_SUCCESS,
        data
    }
}

export function fetchCardsFailure(data){
    return {
        type: FETCHING_CARDS_FAILURE
    }
}

export function getCards(){
    return (dispatch) => {
        dispatch(fetchCards()); //alert someone we are getting synonyms
        return fetch(`http://cfassignment.herokuapp.com/logan/tasks`,
            {
                accept: 'application/json'
            }).then(result => result.json()).then((json) => {
            if (json.error) {
                dispatch(fetchCardsFailure(json.error))
            }  else {
                dispatch(fetchCardsSuccess(json)) //do something with the result
            }
        })
    }
}








export function saveCards(){
    return ({
        type: SAVE_CARDS
    })
}

export function saveCardsSuccess(data){
    return {
        type: SAVE_CARDS_SUCCESS,
        data
    }
}

export function saveCardsFailure(data){
    return {
        type: SAVE_CARDS_FAILURE,
        data
    }
}

export function postCards(){
    return (dispatch, getState) => {
        dispatch(saveCards()); //alert someone we are getting synonyms

        let taskList = getState().cards.map((item) => {
            return item.task
        });

        return fetch(`http://cfassignment.herokuapp.com/logan/tasks`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                },
                body: JSON.stringify({tasks: taskList})
            }).then(result => result.json()).then((json) => {
            if (json.error) {
                dispatch(saveCardsFailure(json.error))
            }  else {
                dispatch(saveCardsSuccess(json)) //do something with the result
            }
        })
    }
}