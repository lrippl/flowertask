import _ from 'lodash';
import {
    ADD_CARD, CARD_ARRAY_MOVE, EDIT_CARD_TITLE, FETCHING_CARDS, FETCHING_CARDS_FAILURE, FETCHING_CARDS_SUCCESS,
    REMOVE_CARD, SAVE_CARDS, SAVE_CARDS_FAILURE, SAVE_CARDS_SUCCESS,
    SET_CARD_TITLE
} from "../constants";
import {arrayMove} from 'react-sortable-hoc';

const initialState = [];

function cardReducer(state = initialState, action) {

    let stateCopy = _.cloneDeep(state);
    console.log(stateCopy);

    // if (!stateCopy) {
    //     stateCopy
    // }

    switch (action.type) {
        case ADD_CARD:
            if (!stateCopy) { //TODO javascript doesnt like this
                stateCopy = [
                    {
                    task: "task",
                    editing: true
                }
                ]
            } else {


                stateCopy.push(
                    {
                        task: "task",
                        editing: true
                    }
                );
            }
            return stateCopy;

        case REMOVE_CARD:
            stateCopy.splice(action.index, 1);
            return stateCopy;

        case SET_CARD_TITLE:
            stateCopy[action.index] = {task: action.text, editing: false};
            return stateCopy;

        case EDIT_CARD_TITLE:
            stateCopy[action.index] = {task: state[action.index].task, editing: true};
            return stateCopy;

        case CARD_ARRAY_MOVE:
            return arrayMove(stateCopy, action.oldIndex, action.newIndex);


        case FETCHING_CARDS:
            //TODO show thing
            return state;

        case FETCHING_CARDS_SUCCESS:
            //TODO show thing

            action.data.tasks.map((task) => {
                stateCopy.push({task: task, editing: false})
            });
            return stateCopy;

        case FETCHING_CARDS_FAILURE:
            //TODO show thing
            return state;


        case SAVE_CARDS:
            //TODO show thing
            return state;

        case SAVE_CARDS_SUCCESS:
            //TODO show thing

            // stateCopy = action.data.tasks;
            return stateCopy;

        case SAVE_CARDS_FAILURE:
            //TODO show thing
            return state;


        default:
            return state;
    }
}

export default cardReducer;