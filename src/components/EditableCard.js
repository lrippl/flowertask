import React, { Component } from 'react';
import Card from "react-toolbox/lib/card/Card";
import CardTitle from "react-toolbox/lib/card/CardTitle";
import {SortableHandle} from "react-sortable-hoc";
import {removeCard} from "../actions/cardActions";
import {connect} from "react-redux";

// TodoList.propTypes = {
//     todos: PropTypes.arrayOf(
//         PropTypes.shape({
//             id: PropTypes.number.isRequired,
//             completed: PropTypes.bool.isRequired,
//             text: PropTypes.string.isRequired
//         }).isRequired
//     ).isRequired,
//     onTodoClick: PropTypes.func.isRequired
// }

{/*<i class="fa fa-ellipsis-v"/>*/}
//<i className="fa fa-th"/>
const DragHandle = SortableHandle(() => <i className="fa fa-th"/>); // This can be any component you want

class EditableCard extends Component {

    constructor(props) {
        super(props);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    handleKeyPress(e) {
        if (e.key === 'Enter') {

            let newText = e.target.value;

            if (!newText) {
                newText = "TASK"
            }

            this.props.setCardTitle(newText, this.props.superIndex)
        }
    };

    render() {
            return (
                <Card
                    className="card"
                    key={this.props.superIndex}
                >
                    <CardTitle

                    >
                        <DragHandle />
                        { this.props.editing ?
                                <input
                                    autoFocus
                                    type="text"
                                    onKeyPress={this.handleKeyPress}
                                />
                                :
                                <span
                                    onClick={() => this.props.editCardTitle(this.props.superIndex)}
                                >
                                    {this.props.task}
                                </span>
                        }
                        <span
                            onClick={() => this.props.removeCard(this.props.superIndex)}
                        >
                            <i className="fa fa-trash-o"/>
                        </span>
                    </CardTitle>
                </Card>
            )
        }
    }

function mapDispatchToProps(dispatch) {
    return {
        removeCard: (index) => dispatch(removeCard(index)),
    }
}

const ConnectedEditableCard = connect(null, mapDispatchToProps)(EditableCard);


export default ConnectedEditableCard