import React, {Component} from 'react';
import {
    SortableContainer,
    SortableElement,
    arrayMove
} from 'react-sortable-hoc';
import {connect} from "react-redux";
import EditableCard from "./EditableCard";
import {cardArrayMove, editCardTitle, setCardTitle} from "../actions/cardActions";

const SortableItem = SortableElement(({index, superIndex, value, editing, setCardTitle, editCardTitle}) =>
    <li>
        <EditableCard
            task={value}
            editing={editing}
            index={index}
            superIndex={superIndex} //HOC is swallowing up the normal index value, don't have time to investigate
            setCardTitle={(text, index) => setCardTitle(text, index)}
            editCardTitle={(index) => editCardTitle(index)}
        />
    </li>
);

const SortableList = SortableContainer(({items, setCardTitle, editCardTitle}) => {
    return (
        <ul>
            {
                items ? items.map((item, index) => (
                <SortableItem
                    key={`item-${index}`}
                    index={index} //this is HOC index
                    superIndex={index}
                    value={item.task}
                    editing={item.editing}
                    setCardTitle={(text, index) => setCardTitle(text, index)}
                    editCardTitle={(index) => editCardTitle(index)}
                />
            ))
            :
            null
            }
        </ul>
    );
});

class SortableTaskList extends Component {

    // constructor() {
    //     super(props)
    // }

    onSortEnd = ({oldIndex, newIndex}) => {
        this.props.cardArrayMove(oldIndex, newIndex)
    };

    render() {
        return (
            <div className="Task-list">
                <SortableList
                    items={this.props.cards}

                    setCardTitle={(text, index) => this.props.setCardTitle(text, index)}
                    editCardTitle={(index) => this.props.editCardTitle(index)}

                    onSortEnd={this.onSortEnd}
                    useDragHandle={true}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        cards: state.cards
    }
}

function mapDispatchToProps(dispatch) {
    return {
        editCardTitle: (index) => dispatch(editCardTitle(index)),
        setCardTitle: (text, index) => dispatch(setCardTitle(text, index)),
        cardArrayMove: (oldIndex, newIndex) => dispatch(cardArrayMove(oldIndex, newIndex))
    }
}

const ConnectedSortableTaskList = connect(mapStateToProps, mapDispatchToProps)(SortableTaskList);

export default ConnectedSortableTaskList;